INSERT INTO rol (nom_rol,estado_rol) VALUES('Medico',1);
INSERT INTO rol (nom_rol,estado_rol) VALUES('Paciente',1);
INSERT INTO rol (nom_rol,estado_rol) VALUES('Administrador',1);

INSERT INTO especialidad (nom_especialidad,estado_especialidad) VALUES('Prueba1',1);
INSERT INTO especialidad (nom_especialidad,estado_especialidad) VALUES('Prueba2',1);
INSERT INTO especialidad (nom_especialidad,estado_especialidad) VALUES('Prueba3',1);

INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('l@gmail.com','123456',1,1);
INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('luis@gmail.com','123456',1,2);
INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('l@gmail.com','123467',1,1);
INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('luis@gmail.com','123467',1,2);
INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('l@gmail.com','123467',1,1);
INSERT INTO usuario (correo_usuario,contra_usuario,estado_visi_usuario,rol) VALUES('usuario@gmail.com','123456',1,2);

INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Diego','Torres','03476517',1,'08/10/1995','3694545',1);
INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Angel','Marquez','45127845',1,'21/11/2000','954786231',2);
INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Diego','Torres','03476517',1,'08/10/1995','3694545',3);
INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Angel','Marquez','45127845',1,'21/11/2000','954786231',4);
INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Diego','Torres','03476517',1,'08/10/1995','3694545',5);
INSERT INTO paciente (nom_paciente,ape_paciente,dni_paciente,estado_visi_paciente,fech_naci_paciente,telf_paciente,usuario) VALUES('Angel','Marquez','45127845',1,'21/11/2000','954786231',6);