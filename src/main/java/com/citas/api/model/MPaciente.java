package com.citas.api.model;

public class MPaciente {

	private Long id;
	private String nombre;
	private String apellido;
	private String dni;
	private String fechaNacimiento;
	private String telefono;
	private int estadoVisi;
	private Long idUsuario;

	public MPaciente(Long id, String nombre, String apellido, String dni, String fechaNacimiento, String telefono,
			int estadoVisi, Long idUsuario) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.fechaNacimiento = fechaNacimiento;
		this.telefono = telefono;
		this.estadoVisi = estadoVisi;
		this.idUsuario = idUsuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getEstadoVisi() {
		return estadoVisi;
	}

	public void setEstadoVisi(int estadoVisi) {
		this.estadoVisi = estadoVisi;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

}
