package com.citas.api.model;

public class MDiagnostico {

	private Long id;
	private String descripcion;
	private String tratamiento;
	private String indicaciones;
	private Long idCita;

	public MDiagnostico(Long id, String descripcion, String tratamiento, String indicaciones, Long idCita) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.tratamiento = tratamiento;
		this.indicaciones = indicaciones;
		this.idCita = idCita;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getIndicaciones() {
		return indicaciones;
	}

	public void setIndicaciones(String indicaciones) {
		this.indicaciones = indicaciones;
	}

	public Long getIdCita() {
		return idCita;
	}

	public void setIdCita(Long idCita) {
		this.idCita = idCita;
	}

}
