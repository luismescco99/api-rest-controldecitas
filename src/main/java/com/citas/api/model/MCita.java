package com.citas.api.model;

public class MCita {

	private Long id;
	private String fecha;
	private String hora;
	private String estadoCita;
	private int estadoVisi;
	private Long idPaciente;
	private Long idMedico;

	public MCita(Long id, String fecha, String hora, String estadoCita, int estadoVisi, Long idPaciente,
			Long idMedico) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.hora = hora;
		this.estadoCita = estadoCita;
		this.estadoVisi = estadoVisi;
		this.idPaciente = idPaciente;
		this.idMedico = idMedico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstadoCita() {
		return estadoCita;
	}

	public void setEstadoCita(String estadoCita) {
		this.estadoCita = estadoCita;
	}

	public int getEstadoVisi() {
		return estadoVisi;
	}

	public void setEstadoVisi(int estadoVisi) {
		this.estadoVisi = estadoVisi;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Long getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(Long idMedico) {
		this.idMedico = idMedico;
	}

}
