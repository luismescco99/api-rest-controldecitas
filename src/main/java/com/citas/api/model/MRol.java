package com.citas.api.model;

public class MRol {

	private Long idRol;
	private String nomRol;
	private int estadoVisiRol;
	
	public MRol(Long idRol, String nomRol, int estadoVisiRol) {
		super();
		this.idRol = idRol;
		this.nomRol = nomRol;
		this.estadoVisiRol = estadoVisiRol;
	}
	
	public Long getIdRol() {
		return idRol;
	}
	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}
	public String getNomRol() {
		return nomRol;
	}
	public void setNomRol(String nomRol) {
		this.nomRol = nomRol;
	}
	public int getEstadoVisiRol() {
		return estadoVisiRol;
	}
	public void setEstadoVisiRol(int estadoVisiRol) {
		this.estadoVisiRol = estadoVisiRol;
	}
	
	
}
