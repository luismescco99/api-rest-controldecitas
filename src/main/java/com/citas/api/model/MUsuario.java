package com.citas.api.model;

public class MUsuario {

	private Long id;
	private String correo;
	private String contrasena;
	private int estadoVisi;
	private Long idRol;

	public MUsuario(Long id, String correo, String contrasena, int estadoVisi, Long idRol) {
		super();
		this.id = id;
		this.correo = correo;
		this.contrasena = contrasena;
		this.estadoVisi = estadoVisi;
		this.idRol = idRol;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public int getEstadoVisi() {
		return estadoVisi;
	}

	public void setEstadoVisi(int estadoVisi) {
		this.estadoVisi = estadoVisi;
	}

	public Long getIdRol() {
		return idRol;
	}

	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}

}
