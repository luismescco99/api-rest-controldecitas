package com.citas.api.model;

public class MEspecialidad {

	private Long idEspecialidad;
	private String nomEspecialidad;
	private int estadoVisi;
	
	public MEspecialidad(Long idEspecialidad, String nomEspecialidad, int estadoVisi) {
		super();
		this.idEspecialidad = idEspecialidad;
		this.nomEspecialidad = nomEspecialidad;
		this.estadoVisi = estadoVisi;
	}
	
	public Long getIdEspecialidad() {
		return idEspecialidad;
	}
	public void setIdEspecialidad(Long idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}
	public String getNomEspecialidad() {
		return nomEspecialidad;
	}
	public void setNomEspecialidad(String nomEspecialidad) {
		this.nomEspecialidad = nomEspecialidad;
	}
	public int getEstadoVisi() {
		return estadoVisi;
	}
	public void setEstadoVisi(int estadoVisi) {
		this.estadoVisi = estadoVisi;
	}
	
	
}
