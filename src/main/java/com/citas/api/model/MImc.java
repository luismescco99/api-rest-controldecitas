package com.citas.api.model;

public class MImc {

	private Long id;
	private String sexo;
	private int altura;
	private int peso;
	private int edad;
	private String resultado;
	private Long idPaciente;

	public MImc(Long id, String sexo, int altura, int peso, int edad, String resultado, Long idPaciente) {
		super();
		this.id = id;
		this.sexo = sexo;
		this.altura = altura;
		this.peso = peso;
		this.edad = edad;
		this.resultado = resultado;
		this.idPaciente = idPaciente;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long id_paciente) {
		this.idPaciente = id_paciente;
	}

}
