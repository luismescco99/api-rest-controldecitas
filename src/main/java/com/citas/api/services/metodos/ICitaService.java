package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Cita;

public interface ICitaService {
	
	public List<Cita> findAll();

	public Cita findById(Long id);
	
	public Cita saveCita(Cita cita); 

}
