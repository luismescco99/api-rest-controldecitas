package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Medico;

public interface IMedicoService {

	public List<Medico> findAll();

	public Medico findById(Long id);
	
	public Medico saveMedico(Medico medico); 
}
