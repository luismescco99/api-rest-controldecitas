package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Diagnostico;

public interface IDiagnosticoService {

	public List<Diagnostico> findAll();

	public Diagnostico findById(Long id);

	public Diagnostico saveDiagnostico(Diagnostico diagnostico);
}
