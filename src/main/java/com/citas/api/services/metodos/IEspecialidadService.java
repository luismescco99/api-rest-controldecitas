package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Especialidad;

public interface IEspecialidadService {
	
	public List<Especialidad> findAll();

	public Especialidad findById(Long id);
	
	public Especialidad saveEspecialidad(Especialidad especialidad);

}
