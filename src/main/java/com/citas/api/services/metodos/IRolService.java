package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Rol;

public interface IRolService {

	public List<Rol> findAll();

	public Rol findById(Long id);
	
	public Rol saveRol(Rol rol);
	
}
