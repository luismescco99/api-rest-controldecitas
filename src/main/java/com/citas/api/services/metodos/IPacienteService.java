package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Paciente;

public interface IPacienteService {

	public List<Paciente> findAll();

	public Paciente findById(Long id);

	public Paciente savePaciente(Paciente paciente);
}
