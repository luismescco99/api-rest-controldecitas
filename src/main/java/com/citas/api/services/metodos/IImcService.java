package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Imc;

public interface IImcService {

	public List<Imc> findAll();

	public Imc findById(Long id);

	public Imc saveImc(Imc imc);
}
