package com.citas.api.services.metodos;

import java.util.List;

import com.citas.api.dao.entity.Usuario;

public interface IUsuarioService {

	public List<Usuario> findAll();

	public Usuario findById(Long id);
	
	public Usuario saveUsuario(Usuario usuario); 
}
