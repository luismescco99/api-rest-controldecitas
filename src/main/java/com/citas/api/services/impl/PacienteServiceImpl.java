package com.citas.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citas.api.dao.entity.Paciente;
import com.citas.api.dao.repository.IPacienteDao;
import com.citas.api.services.metodos.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService {
	
	@Autowired
	private IPacienteDao pacienteDao;

	@Override
	public List<Paciente> findAll() {
		return (List<Paciente>) pacienteDao.findAll();
	}

	@Override
	public Paciente findById(Long id) {
		return pacienteDao.findById(id).orElse(null);
	}

	@Override
	public Paciente savePaciente(Paciente paciente) {
		return pacienteDao.save(paciente);
	}

}
