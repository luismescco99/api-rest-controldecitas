package com.citas.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citas.api.dao.entity.Rol;
import com.citas.api.dao.repository.IRolDao;
import com.citas.api.services.metodos.IRolService;

@Service
public class RolServiceImpl implements IRolService{
	
	@Autowired
	private IRolDao rolDao;

	@Override
	public List<Rol> findAll() {
		return (List<Rol>) rolDao.findAll();
	}

	@Override
	public Rol findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	public Rol saveRol(Rol rol) {	
		return rolDao.save(rol);
	}

}
