package com.citas.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citas.api.dao.entity.Especialidad;
import com.citas.api.dao.repository.IEspecialidadDao;
import com.citas.api.services.metodos.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

	@Autowired
	private IEspecialidadDao especialidadDao;

	@Override
	public List<Especialidad> findAll() {
		return (List<Especialidad>) especialidadDao.findAll();
	}

	@Override
	public Especialidad findById(Long id) {
		return especialidadDao.findById(id).orElse(null);
	}

	@Override
	public Especialidad saveEspecialidad(Especialidad especialidad) {
		return especialidadDao.save(especialidad);
	}

}
