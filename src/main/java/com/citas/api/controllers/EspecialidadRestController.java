package com.citas.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citas.api.dao.entity.Especialidad;
import com.citas.api.services.metodos.IEspecialidadService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class EspecialidadRestController {

	@Autowired
	private IEspecialidadService especialidadService;

	@GetMapping("/especialidades")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Especialidad> index() {
		List<Especialidad> listEspecialidad = new ArrayList<>();
		List<Especialidad> especialidades = especialidadService.findAll();
		for (Especialidad especialidad : especialidades) {
			if (especialidad.getEstado_especialidad() > 0) {
				listEspecialidad.add(especialidad);
			} 
		}
		return listEspecialidad;
	}

	@GetMapping("/especialidades/{id}")
	public ResponseEntity<?> buscarEspecialidad(@PathVariable Long id) {

		Especialidad especialidad = null;
		Map<String, Object> response = new HashMap<>();

		especialidad = especialidadService.findById(id);

		if (especialidad == null) {
			response.put("mensaje", "Error al consultar la especialidad: ".concat(id.toString()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		response.put("especialidad", especialidad);

		return new ResponseEntity<Especialidad>(especialidad, HttpStatus.OK);
	}

	@PostMapping("/especialidades")
	public ResponseEntity<?> crearEspecialidad(@RequestBody Especialidad especialidad) {

		Map<String, Object> response = new HashMap<>();

		especialidadService.saveEspecialidad(especialidad);

		response.put("mensaje", "La especialidad ha sido creado con éxito");
		response.put("especialidad", especialidad);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/especialidades/{id}")
	public ResponseEntity<?> actualizarEspecialidad(@RequestBody Especialidad especialidad, @PathVariable Long id) {

		Especialidad especialidadActual = especialidadService.findById(id);
		especialidadActual.setNom_especialidad(especialidad.getNom_especialidad());
		especialidadActual.setEstado_especialidad(especialidad.getEstado_especialidad());

		Map<String, Object> response = new HashMap<>();

		especialidadService.saveEspecialidad(especialidadActual);

		response.put("mensaje", "La especialidad ha sido modificado correctamente!");
		response.put("especialidad", especialidadActual);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@DeleteMapping("/especialidades/{id}")
	public ResponseEntity<?> eliminarEspecialidad(@PathVariable Long id) {
		Especialidad especialidadActual = especialidadService.findById(id);
		especialidadActual.setEstado_especialidad(0);

		Map<String, Object> response = new HashMap<>();

		especialidadService.saveEspecialidad(especialidadActual);

		response.put("mensaje", "la Especialidad se elimino con éxito!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
