package com.citas.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citas.api.dao.entity.Rol;
import com.citas.api.dao.entity.Usuario;
import com.citas.api.services.metodos.IRolService;
import com.citas.api.services.metodos.IUsuarioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class UsuarioRestController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private IRolService rolService;
	
	@GetMapping(value = "/usuarios")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Usuario> index(){
		List<Usuario> listarUsuario = new ArrayList<>();
		List<Usuario> usuarios = usuarioService.findAll();
		for(Usuario usuario : usuarios) {
			if (usuario.getEstado_visi_usuario()>0) {
				listarUsuario.add(usuario);
			}
		}
		return listarUsuario;
	}
	
	@GetMapping("/usuarios/{id}")
	public ResponseEntity<?> buscarUsuario(@PathVariable Long id) {
		
		Usuario usuario = null;
		Map<String, Object> response = new HashMap<>();
		
		usuario = usuarioService.findById(id);
	
		if(usuario == null) {
			response.put("mensaje", "Error al consultar el usuario: ".concat(id.toString()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		response.put("usuario", usuario);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
	
	@PostMapping("/usuarios")
	public ResponseEntity<?> crearUsuario(@RequestBody Usuario usuario) {
		
		Map<String, Object> response = new HashMap<>();
		
		Usuario user = usuarioService.saveUsuario(usuario);
		
		response.put("mensaje", "El usuario ha sido creado con éxito");
		response.put("usuario", user);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@PutMapping("/usuarios/{id}")
	public ResponseEntity<?> actualizarUsuario(@RequestBody Usuario usuario,@PathVariable Long id) {
		
		Usuario usuarioActual = usuarioService.findById(id);
		Rol rol = rolService.findById(usuario.getRol().getId_rol());
		
		usuarioActual.setCorreo_usuario(usuario.getCorreo_usuario());
		usuarioActual.setContra_usuario(usuario.getContra_usuario());
		usuarioActual.setEstado_visi_usuario(usuario.getEstado_visi_usuario());
		usuarioActual.setRol(rol);
		
		Map<String, Object> response = new HashMap<>();
		
		usuarioService.saveUsuario(usuarioActual);
		
		response.put("mensaje", "El usuario ha sido modificado correctamente!");
		response.put("usuario", usuarioActual);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<?> eliminarUsuario(@PathVariable Long id) {
		Usuario usuarioActual = usuarioService.findById(id);
		usuarioActual.setEstado_visi_usuario(0);
		
		Map<String, Object> response = new HashMap<>();
		
		usuarioService.saveUsuario(usuarioActual);
		
		response.put("mensaje", "El Usuario se elimino con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
}
