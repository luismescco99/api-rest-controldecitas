package com.citas.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citas.api.dao.entity.Paciente;
import com.citas.api.dao.entity.Usuario;
import com.citas.api.services.metodos.IPacienteService;
import com.citas.api.services.metodos.IUsuarioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class PacienteRestController {
	
	@Autowired
	private IPacienteService pacienteService;
	
	@Autowired
	private IUsuarioService usuarioService;
	

	@GetMapping(value = "/pacientes")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Paciente> index(){
		List<Paciente> listarPaciente = new ArrayList<>();
		List<Paciente> pacientes = pacienteService.findAll();
		for(Paciente paciente : pacientes) {
			if (paciente.getEstado_visi_paciente()>0) {
				listarPaciente.add(paciente);
			}
		}
		return listarPaciente;
	}
	
	@GetMapping("/pacientes/{id}")
	public ResponseEntity<?> buscarPaciente(@PathVariable Long id) {
		
		Paciente usuario = null;
		Map<String, Object> response = new HashMap<>();
		
		usuario = pacienteService.findById(id);
	
		if(usuario == null) {
			response.put("mensaje", "Error al consultar el usuario: ".concat(id.toString()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		response.put("usuario", usuario);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
	
	@PostMapping("/pacientes/{idUsuario}")
	public ResponseEntity<?> crearPaciente(@RequestBody Paciente paciente,@PathVariable Long idUsuario) {
		
		Map<String, Object> response = new HashMap<>();
		Usuario usuario = usuarioService.findById(idUsuario);
		
		Paciente pacienteU = paciente;
		pacienteU.setUsuario(usuario);
		pacienteService.savePaciente(pacienteU);
		
		response.put("mensaje", "El usuario ha sido creado con éxito");
		response.put("paciente", pacienteU);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@PutMapping("/pacientes/{id}")
	public ResponseEntity<?> actualizarPaciente(@RequestBody Paciente paciente,@PathVariable Long id) {
		
		Paciente pacienteActual = pacienteService.findById(id);
		Usuario usuario = usuarioService.findById(paciente.getUsuario().getId_usuario());
		
		pacienteActual.setNom_paciente(paciente.getNom_paciente());
		pacienteActual.setApe_paciente(paciente.getApe_paciente());
		pacienteActual.setDni_paciente(paciente.getDni_paciente());
		pacienteActual.setFech_naci_paciente(paciente.getFech_naci_paciente());
		pacienteActual.setTelf_paciente(paciente.getTelf_paciente());
		pacienteActual.setEstado_visi_paciente(paciente.getEstado_visi_paciente());
		
		pacienteActual.setUsuario(usuario);
		
		Map<String, Object> response = new HashMap<>();
		
		pacienteService.savePaciente(pacienteActual);
		
		response.put("mensaje", "El usuario ha sido modificado correctamente!");
		response.put("paciente", pacienteActual);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/pacientes/{id}")
	public ResponseEntity<?> eliminarPaciente(@PathVariable Long id) {
		Paciente pacienteActual = pacienteService.findById(id);
		pacienteActual.setEstado_visi_paciente(0);
		
		Map<String, Object> response = new HashMap<>();
		
		pacienteService.savePaciente(pacienteActual);
		
		response.put("mensaje", "El Paciente se elimino con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
}
