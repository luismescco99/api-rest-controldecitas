package com.citas.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citas.api.dao.entity.Rol;
import com.citas.api.services.metodos.IRolService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class RolRestController {

	@Autowired
	private IRolService rolService;
	
	@GetMapping(value = "/roles")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Rol> index(){
		List<Rol> listarRol = new ArrayList<>();
		List<Rol> roles = rolService.findAll();
		for(Rol rol : roles) {
			if (rol.getEstado_rol()>0) {
				listarRol.add(rol);
			}
		}
		return listarRol;
	}
	
	@GetMapping("/roles/{id}")
	public ResponseEntity<?> buscarRol(@PathVariable Long id) {
		
		Rol rol = null;
		Map<String, Object> response = new HashMap<>();
		
		rol = rolService.findById(id);
	
		if(rol == null) {
			response.put("mensaje", "Error al consultar el rol: ".concat(id.toString()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		response.put("rol", rol);
		
		return new ResponseEntity<Rol>(rol,HttpStatus.OK);
	}
	
	@PostMapping("/roles")
	public ResponseEntity<?> crearRol(@RequestBody Rol rol) {
		
		Map<String, Object> response = new HashMap<>();
		
		rolService.saveRol(rol);
		
		response.put("mensaje", "El rol ha sido creado con éxito");
		response.put("rol", rol);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@PutMapping("/roles/{id}")
	public ResponseEntity<?> actualizarRol(@RequestBody Rol rol,@PathVariable Long id) {
		
		Rol rolActual = rolService.findById(id);
		rolActual.setNom_rol(rol.getNom_rol());
		rolActual.setEstado_rol(rol.getEstado_rol());
		
		Map<String, Object> response = new HashMap<>();
		
		rolService.saveRol(rolActual);
		
		response.put("mensaje", "El rol ha sido modificado correctamente!");
		response.put("rol", rolActual);
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/roles/{id}")
	public ResponseEntity<?> eliminarRol(@PathVariable Long id) {
		Rol rolActual = rolService.findById(id);
		rolActual.setEstado_rol(0);
		
		Map<String, Object> response = new HashMap<>();
		
		rolService.saveRol(rolActual);
		
		response.put("mensaje", "El Rol se elimino con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
}
