package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Imc;

public interface IImcDao extends CrudRepository<Imc, Long>{

}
