package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

}
