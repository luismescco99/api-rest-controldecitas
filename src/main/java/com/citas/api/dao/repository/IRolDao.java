package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Rol;

public interface IRolDao extends CrudRepository<Rol, Long>{

	
}
