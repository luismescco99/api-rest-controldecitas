package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Especialidad;

public interface IEspecialidadDao extends CrudRepository<Especialidad, Long>{

}
