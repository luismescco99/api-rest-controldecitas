package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Diagnostico;

public interface IDiagnosticoDao extends CrudRepository<Diagnostico, Long>{

}
