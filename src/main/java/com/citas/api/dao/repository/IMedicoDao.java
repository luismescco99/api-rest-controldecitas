package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Medico;

public interface IMedicoDao extends CrudRepository<Medico, Long>{

}
