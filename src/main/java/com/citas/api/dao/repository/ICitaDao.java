package com.citas.api.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.citas.api.dao.entity.Cita;

public interface ICitaDao extends CrudRepository<Cita, Long> {

}
