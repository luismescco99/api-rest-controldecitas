package com.citas.api.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "diagnostico")
public class Diagnostico implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_diagnostico;
	@Column(nullable = false, length = 150)
	private String desc_diagnostico;
	@Column(nullable = true, length = 100)
	private String tratamiento;
	@Column(nullable = true, length = 50)
	private String indicaciones;
	@OneToOne(optional = false)
	@JoinColumn(name = "cita", referencedColumnName = "id_cita", nullable = false)
	private Cita cita;

	public Long getId_diagnostico() {
		return id_diagnostico;
	}

	public void setId_diagnostico(Long id_diagnostico) {
		this.id_diagnostico = id_diagnostico;
	}

	public String getDesc_diagnostico() {
		return desc_diagnostico;
	}

	public void setDesc_diagnostico(String desc_diagnostico) {
		this.desc_diagnostico = desc_diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getIndicaciones() {
		return indicaciones;
	}

	public void setIndicaciones(String indicaciones) {
		this.indicaciones = indicaciones;
	}

	public Cita getCita() {
		return cita;
	}

	public void setCita(Cita cita) {
		this.cita = cita;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
