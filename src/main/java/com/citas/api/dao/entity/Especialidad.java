package com.citas.api.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "especialidad")
public class Especialidad implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_especialidad;
	@Column(length = 50, nullable = false)
	private String nom_especialidad;
	@Column(nullable = false)
	private int estado_especialidad;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "especialidad")
	private List<Medico> listaMedico;

	public Long getId_especialidad() {
		return id_especialidad;
	}

	public void setId_especialidad(Long id_especialidad) {
		this.id_especialidad = id_especialidad;
	}

	public String getNom_especialidad() {
		return nom_especialidad;
	}

	public void setNom_especialidad(String nom_especialidad) {
		this.nom_especialidad = nom_especialidad;
	}

	public int getEstado_especialidad() {
		return estado_especialidad;
	}

	public void setEstado_especialidad(int estado_especialidad) {
		this.estado_especialidad = estado_especialidad;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
