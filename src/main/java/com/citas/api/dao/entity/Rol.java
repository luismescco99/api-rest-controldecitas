package com.citas.api.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "rol")
public class Rol implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_rol;
	@Column(length = 20, nullable = false)
	private String nom_rol;
	@Column(nullable = false)
	private int estado_rol;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rol")
	private List<Usuario> listaUsuario;

	public Long getId_rol() {
		return id_rol;
	}

	public void setId_rol(Long id_rol) {
		this.id_rol = id_rol;
	}

	public String getNom_rol() {
		return nom_rol;
	}

	public void setNom_rol(String nom_rol) {
		this.nom_rol = nom_rol;
	}

	public int getEstado_rol() {
		return estado_rol;
	}

	public void setEstado_rol(int estado_rol) {
		this.estado_rol = estado_rol;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
