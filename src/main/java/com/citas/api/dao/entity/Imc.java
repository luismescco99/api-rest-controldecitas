package com.citas.api.dao.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "imc")
public class Imc implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_imc;
	@Column(nullable = false, length = 1)
	private String sexo_imc;
	@Column(nullable = false)
	private int altura_imc;
	@Column(nullable = false)
	private int peso_imc;
	@Column(nullable = false)
	private int edad;
	@Column(nullable = false, length = 30)
	private String result_imc;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "paciente", referencedColumnName = "id_paciente", nullable = false)
	private Paciente paciente;

	public Long getId_imc() {
		return id_imc;
	}

	public void setId_imc(Long id_imc) {
		this.id_imc = id_imc;
	}

	public String getSexo_imc() {
		return sexo_imc;
	}

	public void setSexo_imc(String sexo_imc) {
		this.sexo_imc = sexo_imc;
	}

	public int getAltura_imc() {
		return altura_imc;
	}

	public void setAltura_imc(int altura_imc) {
		this.altura_imc = altura_imc;
	}

	public int getPeso_imc() {
		return peso_imc;
	}

	public void setPeso_imc(int peso_imc) {
		this.peso_imc = peso_imc;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getResult_imc() {
		return result_imc;
	}

	public void setResult_imc(String result_imc) {
		this.result_imc = result_imc;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
