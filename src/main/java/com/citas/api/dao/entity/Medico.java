package com.citas.api.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "medico")
public class Medico implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_medico;
	@Column(nullable = false, length = 30)
	private String nom_medico;
	@Column(nullable = false, length = 30)
	private String ape_medico;
	@Column(nullable = false, length = 8)
	private String dni_medico;
	@Column(nullable = false, length = 11)
	private String fech_naci_medico;
	@Column(nullable = true, length = 11)
	private String telf_medico;
	@Column(nullable = false)
	private int estado_visi_medico;
	@ManyToOne(optional = false)
	@JoinColumn(name = "especialidad", referencedColumnName = "id_especialidad", nullable = false)
	private Especialidad especialidad;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "usuario", referencedColumnName = "id_usuario", nullable = false)
	private Usuario usuario;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "medico")
	private List<Cita> listaCita;

	public Long getId_medico() {
		return id_medico;
	}

	public void setId_medico(Long id_medico) {
		this.id_medico = id_medico;
	}

	public String getNom_medico() {
		return nom_medico;
	}

	public void setNom_medico(String nom_medico) {
		this.nom_medico = nom_medico;
	}

	public String getApe_medico() {
		return ape_medico;
	}

	public void setApe_medico(String ape_medico) {
		this.ape_medico = ape_medico;
	}

	public String getDni_medico() {
		return dni_medico;
	}

	public void setDni_medico(String dni_medico) {
		this.dni_medico = dni_medico;
	}

	public String getFech_naci_medico() {
		return fech_naci_medico;
	}

	public void setFech_naci_medico(String fech_naci_medico) {
		this.fech_naci_medico = fech_naci_medico;
	}

	public String getTelf_medico() {
		return telf_medico;
	}

	public void setTelf_medico(String telf_medico) {
		this.telf_medico = telf_medico;
	}

	public int getEstado_visi_medico() {
		return estado_visi_medico;
	}

	public void setEstado_visi_medico(int estado_visi_medico) {
		this.estado_visi_medico = estado_visi_medico;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
