package com.citas.api.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "paciente")
public class Paciente implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_paciente;
	@Column(nullable = false, length = 30)
	private String nom_paciente;
	@Column(nullable = false, length = 30)
	private String ape_paciente;
	@Column(nullable = false, length = 10)
	private String dni_paciente;
	@Column(nullable = false, length = 11)
	private String fech_naci_paciente;
	@Column(nullable = false, length = 11)
	private String telf_paciente;
	private int estado_visi_paciente;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "usuario", referencedColumnName = "id_usuario", nullable = false)
	private Usuario usuario;
	@OneToOne(mappedBy = "paciente")
	private Imc imc;
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "paciente")
	private List<Cita> listaCita;

	public Long getId_paciente() {
		return id_paciente;
	}

	public void setId_paciente(Long id_paciente) {
		this.id_paciente = id_paciente;
	}

	public String getNom_paciente() {
		return nom_paciente;
	}

	public void setNom_paciente(String nom_paciente) {
		this.nom_paciente = nom_paciente;
	}

	public String getApe_paciente() {
		return ape_paciente;
	}

	public void setApe_paciente(String ape_paciente) {
		this.ape_paciente = ape_paciente;
	}

	public String getDni_paciente() {
		return dni_paciente;
	}

	public void setDni_paciente(String dni_paciente) {
		this.dni_paciente = dni_paciente;
	}

	public String getFech_naci_paciente() {
		return fech_naci_paciente;
	}

	public void setFech_naci_paciente(String fech_naci_paciente) {
		this.fech_naci_paciente = fech_naci_paciente;
	}

	public String getTelf_paciente() {
		return telf_paciente;
	}

	public void setTelf_paciente(String telf_paciente) {
		this.telf_paciente = telf_paciente;
	}

	public int getEstado_visi_paciente() {
		return estado_visi_paciente;
	}

	public void setEstado_visi_paciente(int estado_visi_paciente) {
		this.estado_visi_paciente = estado_visi_paciente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
