package com.citas.api.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_usuario;
	@Column(nullable = false, length = 45)
	private String correo_usuario;
	@Column(nullable = false, length = 20)
	private String contra_usuario;
	@Column(nullable = false)
	private int estado_visi_usuario;
	@ManyToOne(optional = false)
	@JoinColumn(name = "rol", referencedColumnName = "id_rol", nullable = false)
	private Rol rol;
	@OneToOne(mappedBy = "usuario")
	private Medico medico;
	@OneToOne(mappedBy = "usuario")
	private Paciente paciente;

	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getCorreo_usuario() {
		return correo_usuario;
	}

	public void setCorreo_usuario(String correo_usuario) {
		this.correo_usuario = correo_usuario;
	}

	public String getContra_usuario() {
		return contra_usuario;
	}

	public void setContra_usuario(String contra_usuario) {
		this.contra_usuario = contra_usuario;
	}

	public int getEstado_visi_usuario() {
		return estado_visi_usuario;
	}

	public void setEstado_visi_usuario(int estado_visi_usuario) {
		this.estado_visi_usuario = estado_visi_usuario;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
