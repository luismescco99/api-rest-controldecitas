package com.citas.api.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cita")
public class Cita implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_cita;
	@Column(nullable = false, length = 11)
	private String fecha_cita;
	@Column(nullable = false, length = 15)
	private String hora_cita;
	@Column(nullable = false, length = 20)
	private String estado_cita;
	@Column(nullable = false, length = 100)
	private String sintomas_cita;
	@Column(nullable = false)
	private int estado_visi_cita;
	@ManyToOne(optional = false)
	@JoinColumn(name = "paciente", referencedColumnName = "id_paciente", nullable = false)
	private Paciente paciente;
	@ManyToOne(optional = false)
	@JoinColumn(name = "medico", referencedColumnName = "id_medico", nullable = false)
	private Medico medico;
	@OneToOne(mappedBy = "cita")
	private Diagnostico diagnostico;

	public Long getId_cita() {
		return id_cita;
	}

	public void setId_cita(Long id_cita) {
		this.id_cita = id_cita;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public String getHora_cita() {
		return hora_cita;
	}

	public void setHora_cita(String hora_cita) {
		this.hora_cita = hora_cita;
	}

	public String getEstado_cita() {
		return estado_cita;
	}

	public void setEstado_cita(String estado_cita) {
		this.estado_cita = estado_cita;
	}

	public int getEstado_visi_cita() {
		return estado_visi_cita;
	}

	public void setEstado_visi_cita(int estado_visi_cita) {
		this.estado_visi_cita = estado_visi_cita;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public String getSintomas_cita() {
		return sintomas_cita;
	}

	public void setSintomas_cita(String sintomas_cita) {
		this.sintomas_cita = sintomas_cita;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
